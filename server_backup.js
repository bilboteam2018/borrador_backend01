console.log("Hola Mundo")
var express = require('express');
var app = express();
var port = process.env.PORT|| 3000;

//Para rellenar el Body vacío//
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);

console.log("API Molona escuchando en el puerto" + port);


app.get ("/apitechu/v1",
  function(req,res) {
    // Aconsejabe poner en el log, el método y la ruta pàra saber que ejecuta//
    console.log("GET /apitechu/v1");
    res.send({"msg": "Respuesta desde  Pechu"});

  }
);


//Sesión 24/04/2018//

app.get("/apitechu/v1/users",
  function(req, res){
    console.log("GET /apitechu/v1/users");

    //res.sendFile('usuarios.json', )//
    //__dirname es el directorio acual donde se ejecuta el json//
    // el resultado se deja en el sendFile //

    res.sendFile('usuarios.json', {root: __dirname});
    // se puede obtener el fichero directamente haciendo un require//

    //var users  = require('./usuarios.json');//
    //res.send(users);//
  }
);


//SE CREA UN USUARIO// es importante tener estas trazas al inicio de cada ruta//
app.post("/apitechu/v1/users",
function(req, res){
  console.log("POST /apitechu/v1/users");
  console.log("first_name is: " + req.body.first_name);
  console.log("last_name is: " + req.body.last_name);
  console.log("country is: " + req.body.country);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" :  req.body.last_name,
    "country" :    req.body.country
  };

  var users  = require('./usuarios.json'); //es una array////se reogen los usuarios de un fichero.json en users//
   //Cuando nos conectemos a MLAB los datos llegarán de la BBDD//
  users.push(newUser);//mete en la array otro elemento////Se dá de alta un usuario nuevo en esa array//

  writeUserDataToFile(users);
  console.log("Usuario Creado");
  res.send({"msg":"Usuario Creado"});
  /*
  //libreria fs//
  var fs = require('fs');
  //con stringify convierte un js en un json//
  var jsonUserData = JSON.stringify(users);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
  //fichero que se va a escribir, lo que se va a escribir, tipo de almacenaje//
   function(err) {
     if (err) {
       var msg = "Error al escribir fichero de usuarios"
       console.log(msg);
     }
     else {
       var msg = "ok al escribir fichero de usuarios"
       console.log(msg);
     }
     //Se envia la respuesta de la API del servidor//
     res.send({"msg":msg});//se envia el resultado al servidor//
  }
)
*/

}
);

//Borrado de un Registro//
app.delete("/apitechu/v1/users/:id",
function(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(req.params);
  console.log(req.params.id);

  var users  = require('./usuarios.json');
  users.splice(req.params.id - 1, 1) ;
  writeUserDataToFile(users);
  console.log("Usuario Borrado");
  res.send({"msg":"Usuario borrado"});
}
);

//funtion writeUserDataToFile(data)//

function writeUserDataToFile(data){
//libreria fs//
	var fs = require('fs');
	//con stringify convierte un js en un json//
	var jsonUserData = JSON.stringify(data);

	fs.writeFile("./usuarios.json", jsonUserData, "utf8",
	//fichero que se va a escribir, lo que se va a escribir, tipo de almacenaje//
	 function(err) {
	   if (err) {
		 console.log(err);
	   }
	   else {
		 console.log("ok al escribir fichero de usuarios");
	   }
	}
	)
}



//Sesión 25/04/2018//

//SE prueban los métodos //
app.post("/apitechu/v1/monstruo/:p1/:p2",
function(req, res){
  console.log("Parametros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Body");
  console.log(req.body);

  console.log("Cabeceras");
  console.log(req.headers);
}
)
