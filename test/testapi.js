var mocha=require('mocha');
var chai=require('chai');
var chaihttp=require('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../server');//lanza el server sin necesidad de tener la api iniciada//
                                  // Es lo que se llama test unitario autocontenido//
                                 //First, F-Fast, I-Independant, R-Repitable S-Self_validating, T-Timely//

//se prueban unidades de software, classes etc//
//First test es una swich//

// Se comprueba con la llamada a esta http nos devuelve los resultados del test//
describe('First test',
 function(){
   it('Test tahat DuckDuckGo works',
      function(done){
        chai.request('http://www.duckduckgo.com')
            .get('/')
            .end(
                function(err, res){
                  console.log("Request has ended");
                  console.log(err);
                      //console.log(res);//
                  res.should.have.status(200);//se guarda la respuesta como código 200 para saber que lo ha hecho bien//
                                                  //el codigo de rectorno ok es 200, se comprueba que ho hace bien y no hay falsos positivos//
                  done();// se usa para que sepa el final de las aserciones//
                }
            )

      }
   )
 }
 );



 describe('Test de API de usuarios TechU',
  function(){
    it('Test de API de usuarios TechU',
       function(done){
         chai.request('http://localhost:3000')
             .get('/apitechu/v1')
             .end(
                 function(err, res){
                   console.log("Request has ended");
                   //console.log(res);//
                       res.should.have.status(200);//se guarda la respuesta como código 200 para saber que lo ha hecho bien//
                       res.body.msg.should.be.eql("Respuesta desde  Pechu")   ;                        //el codigo de rectorno ok es 200, se comprueba que ho hace bien y no hay falsos positivos//
                       done();// se usa para que sepa el final de las aserciones//
                 }
             )

       }
    ),
    it('Prueba que la API devuelve una lista de usuarios correcta',
       function(done){
         chai.request('http://localhost:3000')
             .get('/apitechu/v1/users')
             .end(
                 function(err, res){
                   console.log("Request has ended");
                   //console.log(res);//
                       res.should.have.status(200);//se guarda la respuesta como código 200 para saber que lo ha hecho bien//
                       res.body.should.be.a("array");//a, es un tipo primitivo, el formaton en este caso es un array//
                      for  (user of res.body){
                        user.should.have.property('email');
                        user.should.have.property('password');
                      }

                        done();
                 }
             )

       }
    )

  }
  );
