FROM node

WORKDIR /apitechu

#mete el codigo en apitechu
ADD . /apitechu

#Se abre un puerto
EXPOSE 3000

#En el directorio de trabajo ponemos npm start para que la API funcione
CMD ["npm", "start"]
