 console.log("Hola Mundo")
var express = require('express');
var app = express();
var port = process.env.PORT|| 3000;

//Para rellenar el Body vacío//
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMlabURL = "https://api.mlab.com/api/1/databases/mlab_sfr/collections/";
var mlabApkikey = "apiKey=wAoGePQuuckkWWi0Q3xbsR9DKjWjJrJo";
var requestJson =  require('request-json');

//apikey={{EN}}// Forma de poner las variables de entorno.

app.listen(port);

console.log("API Molona escuchando en el puerto" + port);


app.get ("/apitechu/v1",
  function(req,res) {
    // Aconsejabe poner en el log, el método y la ruta pàra saber que ejecuta//
    console.log("GET /apitechu/v1");
    res.send({"msg": "Respuesta desde  Apitechu"});

  }
);




//LOGIN CON MLAB//
app.put("/apitechu/v2/login",
function(req, res){
  console.log("put /apitechu/v2/login");
  console.log("Email is: " + req.body.Email);
  console.log("Password is: " +  req.body.Password);

  var newUser = {
    "Email": req.body.Email,
    "Password" : req.body.Password
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

      var query =  'q={"Email":"' + newUser.Email + '", "Password":"' + newUser.Password +'"}';
      //var putBody =  '{"$set": {"logged":true}}';//
      var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
  //ar users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//



          function(err, resMlab, body) {
              //err:400 es un error del servidor//
              //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
              //body , arrary de la tabla//

              //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
              //var response = !err ? body : {
              //  "msg": "Error obteniendo usuario."

              var response = {};

                if (err) {
                  response = {
                        "msg": "Error obtendiendo el usuario"
                  }
                  res.status(500);

                } else {

                    if (body.length > 0){
                        //response =body;
                        var putBody = '{"$set":{"logged": true}}'
                        httpClient.put("Usuarios?" + query + "&" + mlabApkikey, JSON.parse(putBody));

                        response ={
                            "msg": "Usuario encontrado",
                            "id": body[0].id,
                            "Avatar" : body[0].Avatar
                        };
                        res.status(200);

                      } else {
                        response ={
                            "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                    }
                    res.send(response);
              }
          }
  )
}
);



//LOGOUT CON MLAB//
//
//
app.put("/apitechu/v2/logout",
function(req, res){
  console.log("POST /apitechu/v2/logout");
  console.log("id is: " + req.body.id);
  console.log(req.body);

  var newUser = {
    "id": req.body.id
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

      var query =  'q={"id":' + newUser.id + '}';
      var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
  //ar users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//



          function(err, resMlab, body) {
              //err:400 es un error del servidor//
              //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
              //body , arrary de la tabla//

              //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
              //var response = !err ? body : {
              //  "msg": "Error obteniendo usuario."

              var response = {};

                if (err) {
                  response = {
                        "msg": "Error obtendiendo el usuario"
                  }
                  res.status(500);

                } else {
                    if (body.length > 0){
                        //response =body;
                        var putBody = '{"$unset":{"logged": ""}}'
                        httpClient.put("Usuarios?" + query + "&" + mlabApkikey, JSON.parse(putBody));

                        response ={
                            "msg": "Log out Correcto",
                            "idUsuario": body[0].id
                        };

                      } else {
                        response ={
                            "msg": "Error  usuario no encontrado"
                      };
                      res.status(404);
                    }

              }
            res.send(response);
          }

  )
}
);

// GET CONTRA LA BASE DE DATOS V2 OBTENER TODOS LOS USUARIOS//
app.get("/apitechu/v2/Usuarios",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios");

    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("Usuarios?" + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo usuarios"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error usuarios no encontrados"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);

// GET CONTRA LA BASE DE DATOS V2 OBTENER EL USUARIO POR ID//
app.get("/apitechu/v2/Usuarios/:id",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id");

    // se recoge la id//
    var id = req.params.id;
    //se crea la consulta//
    var query =  'q={"id":' + id + '}';


    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");


    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);

// GET CONTRA LA BASE DE DATOS V2 OBTENER LAS CUENTAS POR ID//
app.get("/apitechu/v2/Usuarios/:id/Cuentas",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id/Cuentas");

    // se recoge la id//
    var id = req.params.id;
    //se crea la consulta//
    var query =  'q={"id":' + id + '}';
    console.log(id);
    console.log(query);

    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");


    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Toda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


// GET CONTRA LA BASE DE DATOS V2 OBTENER LOS MOVIMIENTOS DE LAS CUENTAS POR USUARIO//
 app.get("/apitechu/v2/Usuarios/:id/:idCuenta/Cuentas",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id/:idCuenta/Cuentas");

    // se recoge la id//

      var id = req.params.id;
      var idCuenta=req.params.idCuenta;



   console.log(idCuenta);
    //se crea la consulta//

    //+ ',' + '{' "idCuenta:" + id_idCuenta.idCuenta + '}'
    var query =  'q={"id":' + id + '},' + '{' + "idCuenta:" + idCuenta + '}';
    console.log(id);
    console.log(idCuenta);
    console.log(query);

    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Toda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


//SIGNUP CON MLAB//
app.post("/apitechu/v2/signup",
  function(req, res){
    console.log("POST /apitechu/v2/signup");

    var email = req.body.Email;

    var query = 'q={"Email":"' + email + '"}';
    var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
      function(err, resMlab, body) {
          //err:400 es un error del servidor//
          //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
          //body , arrary de la tabla//

          //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
          //var response = !err ? body : {
          //  "msg": "Error obteniendo usuario."

          var response = {};

          if (err) {
            response = {
              "msg": "Error realizando el signup del usuario"
            }
            res.status(500);

          } else {
            if (body.length === 0) {
              //response =body;
              var query_id = 's={"id":-1}&l=1';
              var id = 0;

              httpClient.get("Usuarios?" + query_id + "&" + mlabApkikey,
                function(err1, resMlab1, body1) {
                  id = body1[0].id + 1;
                  console.log(id);

                  var newUser = {
                    "id": id,
                    "Email": req.body.Email,
                    "Password" : req.body.Password,
                    "Nombre": req.body.Nombre,
                    "Apellido1": req.body.Apellido1,
                    "Apellido2": req.body.Apellido2,
                    "Sexo": req.body.Sexo,
                    "Avatar": req.body.Avatar,
                    "Edad": req.body.Edad,
                    "Direccion": req.body.Direccion
                  };
                  console.log(newUser)

                  var putBody = '{"id":'+newUser.id+',"Email":"'+newUser.Email+'","Password":"'+newUser.Password+
                        '","Nombre":"'+newUser.Nombre+'","Apellido1":"'+newUser.Apellido1+'","Apellido2":"'+newUser.Apellido2+
                        '","Sexo":"'+newUser.Sexo+'","Avatar":"'+newUser.Avatar+
                        '","Edad":'+newUser.Edad+',"Direccion":"'+newUser.Direccion +'"}'

                  httpClient.post("Usuarios?" + mlabApkikey, JSON.parse(putBody));
                  console.log("post lanzado");
                  response ={
                    "msg": "Usuario dado de alta",
                    "id" : newUser.id
                  };
                  res.status(200);
                  res.send(response);
                }
              )
            }
            else {
              console.log("El usuario ya existe");
              response = {
                "msg": "Error! El usuario ya existe"
              }
              res.status(404);
            }
          }
      }
    )
  }
);

//Update Perfil CON MLAB//
app.put("/apitechu/v2/perfil",
  function(req, res){
    console.log("PUT /apitechu/v2/perfil");

    var email = req.body.Email;

    var query = 'q={"Email":"' + email + '"}';
    var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
      function(err, resMlab, body) {
          //err:400 es un error del servidor//
          //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
          //body , arrary de la tabla//

          //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
          //var response = !err ? body : {
          //  "msg": "Error obteniendo usuario."

          var response = {};

          if (err) {
            response = {
              "msg": "Error realizando el signup del usuario"
            }
            res.status(500);

          } else {
            if (body.length > 0) {
              //response =body;
              var newUser = {
                "id": req.body.id,
                "Email": req.body.Email,
                "Password" : req.body.Password,
                "Nombre": req.body.Nombre,
                "Apellido1": req.body.Apellido1,
                "Apellido2": req.body.Apellido2,
                "Sexo": req.body.Sexo,
                "Avatar": req.body.Avatar,
                "Edad": req.body.Edad,
                "Direccion": req.body.Direccion
              };
              console.log(newUser)

              var putBody = '{"id":'+newUser.id+',"Email":"'+newUser.Email+'","Password":"'+newUser.Password+
                    '","Nombre":"'+newUser.Nombre+'","Apellido1":"'+newUser.Apellido1+'","Apellido2":"'+newUser.Apellido2+
                    '","Sexo":"'+newUser.Sexo+'","Avatar":"'+newUser.Avatar+
                    '","Edad":'+newUser.Edad+',"Direccion":"'+newUser.Direccion+'"}'

              var query_id = 'q={"id":' + newUser.id + '}';

              httpClient.put("Usuarios?" + query_id + "&" + mlabApkikey, JSON.parse(putBody));
              console.log("put lanzado");

              response ={
                "msg": "Usuario actualizado",
                "id" : newUser.id
              };
              res.status(200);
              res.send(response);
            }
            else {
              console.log("Error al insertar el usuario");
              response = {
                "msg": "Error!"
              }
              res.status(404);
            }
          }
      }
    )
  }
);


app.put("/apitechu/v2/putuser",
function(req, res){
  console.log("PUT /apitechu/v2/putuser");
  console.log("email is: " + req.body.Email);

  var email = req.body.Email;

  var query = 'q={"Email":"' + email + '"}';
  var httpClient= requestJson.createClient(baseMlabURL);

  httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
    function(err, resMlab, body) {
      //err:400 es un error del servidor//
      //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
      //body , arrary de la tabla//

      //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
      //var response = !err ? body : {
      //  "msg": "Error obteniendo usuario."

      var response = {};

      if (err) {
        response = {
          "msg": "Error realizando el delete del usuario"
        }
        console.log("error");
        res.status(500);

      } else {
        if (body.length > 0) {
          response = {
            "msg": "Usuario borrado"
          }

          var putBody = '{}'
          httpClient.put("Usuarios?" + query + "&" + mlabApkikey, JSON.parse(putBody));
        }
        else{
          response = {
            "msg": "Usuario no existe"
          }
          console.log("Usuario no existe");
        }

        res.status(404);
      }

  console.log("Fin");
  res.send(response);
  }
);
}
);
