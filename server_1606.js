console.log("Hola Mundo")
var express = require('express');
var app = express();
var port = process.env.PORT|| 3000;

//Para rellenar el Body vacío//
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMlabURL = "https://api.mlab.com/api/1/databases/mlab_sfr/collections/";
var mlabApkikey = "apiKey=wAoGePQuuckkWWi0Q3xbsR9DKjWjJrJo";
var requestJson =  require('request-json');

//apikey={{EN}}// Forma de poner las variables de entorno.

app.listen(port);

console.log("API Molona escuchando en el puerto" + port);






//SE CREA UN USUARIO// es importante tener estas trazas al inicio de cada ruta//
app.post("/apitechu/v1/login",
function(req, res){
  console.log("POST /apitechu/v1/login");
  console.log("email is: " + req.body.email);
  console.log("pass is: " +  req.body.password);

  var newUser = {
    "email": req.body.email,
    "pass" : req.body.password
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

  var users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//

  for (user of users) {
    if ((newUser.email == user.email) && (newUser.pass == user.password)){

    console.log("Login Correcto");

    var_msg = {
      "mensaje":"Login Correcto",
      "idUsuario":user.id
    }


    user.logged = true;
    writeUserDataToFile(users);
    break;
    }

    else {
      console.log("Login Incorrecto");

    }

  }

  console.log("Fin");
  res.send(var_msg);
  }
);



//Borrado de un Registro//
app.delete("/apitechu/v1/users/:id",
function(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(req.params);
  console.log(req.params.id);

  var users  = require('./usuarios.json');
  users.splice(req.params.id - 1, 1) ;
  writeUserDataToFile(users);
  console.log("Usuario Borrado");
  res.send({"msg":"Usuario borrado"});
}
);

//funtion writeUserDataToFile(data)//

function writeUserDataToFile(data){
//libreria fs//
	var fs = require('fs');
	//con stringify convierte un js en un json//
	var jsonUserData = JSON.stringify(data);

	fs.writeFile("./usuarios.json", jsonUserData, "utf8",
	//fichero que se va a escribir, lo que se va a escribir, tipo de almacenaje//
	 function(err) {
	   if (err) {
		 console.log(err);
	   }
	   else {
		 console.log("ok al escribir fichero de usuarios");
	   }
	}
	)
}



//Sesión 25/04/2018//

//SE prueban los métodos //
app.post("/apitechu/v1/monstruo/:p1/:p2",
function(req, res){
  console.log("Parametros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Body");
  console.log(req.body);

  console.log("Cabeceras");
  console.log(req.headers);
}
)






// GET CONTRA LA BASE DE DATOS V2 OBTENER EL USUARIO POR ID//
app.get("/apitechu/v2/Usuarios/:id",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id");

    // se recoge la id//
    var id = req.params.id;
    //se crea la consulta//
    var query =  'q={"id":' + id + '}';


    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");


    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


//LOGIN CON MLAB//
app.put("/apitechu/v2/login",
function(req, res){
  console.log("put /apitechu/v2/login");
  console.log("Email is: " + req.body.Email);
  console.log("Password is: " +  req.body.Password);

  var newUser = {
    "Email": req.body.Email,
    "Password" : req.body.Password
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

      var query =  'q={"Email":"' + newUser.Email + '", "Password":"' + newUser.Password +'"}';
      //var putBody =  '{"$set": {"logged":true}}';//
      var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
  //ar users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//



          function(err, resMlab, body) {
              //err:400 es un error del servidor//
              //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
              //body , arrary de la tabla//

              //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
              //var response = !err ? body : {
              //  "msg": "Error obteniendo usuario."

              var response = {};

                if (err) {
                  response = {
                        "msg": "Error obtendiendo el usuario"
                  }
                  res.status(500);

                } else {

                    if (body.length > 0){
                        //response =body;
                        var putBody = '{"$set":{"logged": true}}'
                        httpClient.put("Usuarios?" + query + "&" + mlabApkikey, JSON.parse(putBody));

                        response ={
                            "msg": "Usuario encontrado",
                            "id": body[0].id,
                            "Avatar" : body[0].Avatar
                        };

                      } else {
                        response ={
                            "msg": "Error  usuario no encontrado"
                      };
                      res.status(404);
                    }

              }
            res.send(response);
          }

  )
}
);



//LOGOUT CON MLAB//
//
//
app.put("/apitechu/v2/logout",
function(req, res){
  console.log("POST /apitechu/v2/logout");
  console.log("id is: " + req.body.id);
  console.log("idC is: " + req.body.idCliente);
  console.log(req.body);

  var newUser = {
    "id": req.body.id
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

      var query =  'q={"id":' + newUser.id + '}';
      var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("user?" + query + "&" + mlabApkikey,
  //ar users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//



          function(err, resMlab, body) {
              //err:400 es un error del servidor//
              //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
              //body , arrary de la tabla//

              //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
              //var response = !err ? body : {
              //  "msg": "Error obteniendo usuario."

              var response = {};

                if (err) {
                  response = {
                        "msg": "Error obtendiendo el usuario"
                  }
                  res.status(500);

                } else {
                    if (body.length > 0){
                        //response =body;
                        var putBody = '{"$unset":{"logged": ""}}'
                        httpClient.put("user?" + query + "&" + mlabApkikey, JSON.parse(putBody));

                        response ={
                            "msg": "Log out Correcto",
                            "idUsuario": body[0].id
                        };

                      } else {
                        response ={
                            "msg": "Error  usuario no encontrado"
                      };
                      res.status(404);
                    }

              }
            res.send(response);
          }

  )
}
);


// GET CONTRA LA BASE DE DATOS V2 OBTENER LAS CUENTAS POR ID//
app.get("/apitechu/v2/Usuarios/:id/Cuentas",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id/Cuentas");

    // se recoge la id//
    var id = req.params.id;
    //se crea la consulta//
    var query =  'q={"id":' + id + '}';


    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");


    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Toda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


// GET CONTRA LA BASE DE DATOS V2 OBTENER LOS MOVIMIENTOS DE LAS CUENTAS POR USUARIO//
app.get("/apitechu/v2/Usuarios/:id/:idCuenta/Cuentas",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id/:idCuenta/Cuentas");

    // se recoge la id//
    var id_idCuenta ={
     "id": req.body.id,
     "idcuenta":req.body.idCuenta
    }


    //se crea la consulta//

    var query =  'q={"id":' + id_idCuenta.id + , "idCuenta:" + id_idCuenta.idCuenta + '}';
    console.log(id);
    console.log(idCuenta);
    console .log(id_idCuenta);
    console.log(query);
    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Toda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


//SIGNUP CON MLAB//
app.post("/apitechu/v2/signup",
  function(req, res){
    console.log("POST /apitechu/v2/signup");

    var email = req.body.email;

    var query = 'q={"email":"' + email + '"}';
    var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("user?" + query + "&" + mlabApkikey,
      function(err, resMlab, body) {
          //err:400 es un error del servidor//
          //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
          //body , arrary de la tabla//

          //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
          //var response = !err ? body : {
          //  "msg": "Error obteniendo usuario."

          var response = {};

          if (err) {
            response = {
              "msg": "Error realizando el signup del usuario"
            }
            res.status(500);

          } else {
            if (body.length === 0) {
              //response =body;
              var query_id = 's={"id":-1}&l=1';
              var id = 0;

              httpClient.get("user?" + query_id + "&" + mlabApkikey,
                function(err1, resMlab1, body1) {
                  id = body1[0].id + 1;
                  console.log(id);

                  var newUser = {
                    "id": id,
                    "email": req.body.email,
                    "password" : req.body.password,
                    "nombre": req.body.nombre,
                    "apellido1": req.body.apellido1,
                    "apellido2": req.body.apellido2,
                    "sexo": req.body.sexo,
                    "avatar": req.body.avatar,
                    "edad": req.body.edad,
                    "poblacion": req.body.poblacion
                  };
                  console.log(newUser)

                  var putBody = '{"id":'+newUser.id+',"email":"'+newUser.email+'","password":"'+newUser.password+
                        '","nombre":"'+newUser.nombre+'","apellido1":"'+newUser.apellido1+'","apellido2":"'+newUser.apellido2+
                        '","sexo":"'+newUser.sexo+'","avatar":"'+newUser.avatar+
                        '","edad":'+newUser.edad+',"poblacion":"'+newUser.poblacion+'"}'

                  httpClient.post("user?" + mlabApkikey, JSON.parse(putBody));
                  console.log("post lanzado");
                }
              )
              response ={
                "msg": "Usuario dado de alta"
              };
            }
            else {
              console.log("El usuario ya existe");
              response = {
                "msg": "Error! El usuario ya existe"
              }
            }
            res.status(404);

          }
        res.send(response);
      }
    )
  }
);

app.put("/apitechu/v2/putuser",
function(req, res){
  console.log("PUT /apitechu/v2/putuser");
  console.log("email is: " + req.body.email);

  var email = req.body.email;

  var query = 'q={"email":"' + email + '"}';
  var httpClient= requestJson.createClient(baseMlabURL);

  httpClient.get("user?" + query + "&" + mlabApkikey,
    function(err, resMlab, body) {
      //err:400 es un error del servidor//
      //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
      //body , arrary de la tabla//

      //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
      //var response = !err ? body : {
      //  "msg": "Error obteniendo usuario."

      var response = {};

      if (err) {
        response = {
          "msg": "Error realizando el delete del usuario"
        }
        console.log("error");
        res.status(500);

      } else {
        if (body.length > 0) {
          response = {
            "msg": "Usuario borrado"
          }
          console.log("ok");
          var query_id = 'q={"_id":"' + body[0]._id + '"}';
          console.log(body[0])
          var putBody = '{}'
          httpClient.put("user?" + query_id + "&" + mlabApkikey, JSON.parse(putBody));
        }
        else{
          response = {
            "msg": "Usuario no existe"
          }
          console.log("Usuario no existe");
        }

        res.status(404);
      }

  console.log("Fin");
  res.send(response);
  }
);
}
);
